//TODO: Test on Android and iOS devices
//TODO: Fix border bounce back transition on swipe

(function($) {
  'use strict';

  $.extend(csSliderousel, {
    _initTouchEnabled: function() {
      var self = this;

      self._dragDelta = self.opt['speed'];
      self._friction = 0.35;
      self._mouseDown = false;

      //hook into the system function
      self.el.on('beforeInit', function() {
        self.el.addClass('no-touch');
      });

      //Start the mouse delta on mouse down
      //For this event, calculate the X-axis difference
      //If greater that 150pixel, advance slides
      self.slideContainer.on('touchstart.csSliderousel', function(e) {
        var target = e.target || e.srcElement;

        self.el.removeClass('no-touch');

        self.slides.blur();

        //prevents the browser from trying to drag the image
        //if(target.nodeName === 'IMG') {
          //e.preventDefault();
        //}
       
        self.touchDelta = e.clientX || e.originalEvent.changedTouches[0].pageX;
        self.startSwipe = 0;

        if(self.opt['animation'] !== 'fade') {
          if(self._isCssAnimation) {
            var matrix = $(self.slides.eq(0)).css(self.helpers.getVendorPrefix('transform')).split(',');
            self.startLeft = parseInt(matrix[4], 10);
          } else {
            self.startLeft = parseInt(self.sliderMask.css('left'), 10) || 0;
          }
        }
       
        self._mouseDown = true;

        if(target.nodeName === 'OBJECT') {
          self._mouseDown = false; //quick fix for flash videos
        }
      });

      self.slideContainer.on('mousemove.csSliderousel', function() {
        self.el.addClass('no-touch');
      });

      $('body, html').on('touchmove.csSliderousel', '[data-id='+self.el.attr('data-id')+']', function(e) {
        var target = e.target || e.srcElement;

        //Prevents the animation from not accounting for borders using touch events
        self.slides.blur();
        self.slideWrapper.blur();
        self.slides.removeClass('cs-hover');

        if(target.nodeName === 'OBJECT') {
          self._mouseDown = false; //quick fix for flash videos
        }

        if(self._mouseDown) {
          self._isDragging = true;
          (self.startSwipe === 0) ? self.startSwipe = Date.now() : 0;

          self.tempTouchDelta = Math.abs(self.touchDelta - (e.clientX || e.originalEvent.changedTouches[0].pageX));
          
          //Allow page scrolling for Android if touchDeltaX is greater than 15 pixels
          if(self.tempTouchDelta > 7) {
            e.preventDefault();
            e.stopPropagation();
          }

          if(self.opt['animation'] !== 'fade') {
            if(self._isCssAnimation) {
              self.slides.css(self.helpers.getVendorPrefix('transition'), '0ms cubic-bezier(0.39, 0.575, 0.565, 1)');
              self.slides.css(self.helpers.getVendorPrefix('transform'), 'translate3d(' + (self.startLeft - (self.touchDelta - (e.clientX || e.originalEvent.changedTouches[0].pageX)) * self._friction) + 'px, 0, 0)');
            } else {
              self.sliderMask.css('left', self.startLeft - (self.touchDelta - (e.clientX || e.originalEvent.changedTouches[0].pageX)));
            }
          }
        }
      });

      $('body').on('touchend.csSliderousel', '[data-id=' + self.el.attr('data-id') + ']', function(e) {
        //e.stopPropagation();       

        if(self.tempTouchDelta <=7) { self._isDragging = false; self._mouseDown = false; return false; }
        
        //minimum swipe speed is 100ms, maximum 500ms
        //self._swipeTimeDelta = (Date.now() - self.startSwipe) > 400 ? self.opt['speed'] : self.opt['speed'] - (Date.now() - self.startSwipe);

        if(self.opt['animation'] !== 'fade') {

          if(self._isCssAnimation) {
            var matrix = $(self.slides.eq(0)).css(self.helpers.getVendorPrefix('transform')).split(',') || 'matrix(1, 0, 0, 1, 0, 0)';
            
            matrix = (matrix === 'none') ? 'matrix(1, 0, 0, 1, 0, 0)' : matrix;

            self._swipeTimeDelta = self.opt['speed'] - (Math.abs(Math.abs(parseInt(matrix[4], 10)) - (self._slideIndex * self.slides.eq(0).width())) / self.slides.eq(0).width() * self.opt['speed']);
          } else {
            self._swipeTimeDelta = self.opt['speed'] - (Math.abs(Math.abs(parseInt(self.sliderMask.css('left'), 10)) - (self._slideIndex * self.slides.eq(0).width())) / self.slides.eq(0).width() * self.opt['speed']);
          }
        }

        if(self._isDragging) {
          self.touchDelta = self.touchDelta - (e.clientX || e.originalEvent.changedTouches[0].pageX);

          if(self.touchDelta > 30) {
            self.next();
          } else if(self.touchDelta < -30) {
            self.previous();
          } else {
            self.goToSlide(self._slideIndex);
          }
          self._isDragging = false;
          //self.helpers.forceWebkitRepaint();
        }

        self._mouseDown = false;
      });
    }
  });

  $.fn['csSliderousel']['modules'].touchEnabled = csSliderousel._initTouchEnabled;

}(jQuery));