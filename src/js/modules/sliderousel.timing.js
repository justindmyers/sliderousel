(function($) {
  'use strict';

  $.extend(csSliderousel, {
    _initTimingModule: function() {
      var self = this;

      self._timing = 0;
      self._percent = 0;
      self._pauseTime = 0;
      self._isDragging = false;
    },

    /**
     * Update the timer
     * @param  {string} type The event to update the timer by
     */
    _updateTimerContainer: function(type) {
      var self = this;
      if(self.opt['timer']) {
        switch(type) {
        case 'start':
          self.timerContainer.children('span')
            .css('width', Math.round(self._percent * 100) + '%')
            .stop()
            .animate({
              width: '100%'
            }, self.opt['timeOut'] - self._pauseTime, 'linear');
          break;

        case 'restart':
          self.timerContainer.children('span')
            .stop()
            .css('width', 0 + '%')
            .animate({
              width: '100%'
            }, self.opt['timeOut'], 'linear');
          break;

        case 'stop':
          self.timerContainer.children('span').css('width', '0%').stop();
          break;

        case 'pause':
          self.timerContainer.children('span').stop(); //stop all animation
          break;
        }
      }
    },

    timing: function() {
      var self = this;

      return {
        /**
         * [_startTimer Start the slider timer]
         * @param  {[type]} time [description]
         * @return {[type]}      [description]
         */
        _startTimer: function() {
          //If paused, unpause the slider else just reset the pauseTime
          self._paused ? self._paused = false : self._pauseTime = 0;
          self.startTime = self._tick = Date.now();

          //Clear the timer just in case
          self.timer && window.cancelAnimationFrame(self.timer);
          self.timer = window.requestAnimationFrame(this._getAnimationFrame.bind(self));

          self._updateTimerContainer('start');
        },

        /**
         * [_restartTimer Restart the slider timer]
         * @param  {[type]} stop [description]
         * @return {[type]}      [description]
         */
        _restartTimer: function(stop) {
          self._pauseTime = 0;
          window.cancelAnimationFrame(self.timer);

          if(!stop) {
            self.timing._startTimer();
          } else {
            self.timing._pauseTimer();
            self._tick = Date.now();
            self.timing._updateTime();
          }

          self._updateTimerContainer('restart');
        },

        /**
         * [_stopTimer Stop the slider timer]
         * @return {[type]} [description]
         */
        _stopTimer: function() {
          window.cancelAnimationFrame(self.timer);
          self._paused = true;
          self._pauseTime = 0;
          self.startTime = Date.now();

          self._updateTimerContainer('stop');
        },

        /**
         * [_pauseTimer Pause the slider timer]
         * @return {[type]} [description]
         */
        _pauseTimer: function() {
          window.cancelAnimationFrame(self.timer);
          self._paused = true;
          self._pauseTime = Date.now() - self.startTime + self._pauseTime; //get the currently elapsed time, add any already paused time to it

          self._updateTimerContainer('pause');
        },

        /**
         * [_updateTime Update the slider timer]
         * @return {[type]} [description]
         */
        _updateTime: function() {
          self._percent = (self._tick - self.startTime) / (self.opt['timeOut']);

          if(self._percent >= 0) {
            if(self._pauseTime > 0) {
              self._percent += (self._pauseTime / self.opt['timeOut']);
            }

            self._percent > 1 && (self._percent = 1); //max out percent at 100
          }
        },
        _getAnimationFrame: function() {
          if(!this._paused) {
            this._tick = Date.now();
            this.timing._updateTime();

            if(this._percent >= 1) {
              this.next();
              !this._paused && this.timing._restartTimer(); //restart the timer if not paused
            }

            //Keep calling _getAnimationFrame
            window.requestAnimationFrame(this.timing._getAnimationFrame.bind(this));
          }
        }
      };
    }
  });

  $.fn['csSliderousel']['modules'].timing = csSliderousel._initTimingModule;
}(jQuery));
