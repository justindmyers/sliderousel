(function($) {
  'use strict';

  $.fn['csSliderousel']._activePage = function() {
    
    var self = this;

    self.el.on('afterInit', function() {
      var currentLocation = window.location.href.toLowerCase();

      self.slides.removeClass('active');

      self.slides.each(function(){
        typeof(this.href) !== 'undefined' && currentLocation.indexOf(this.href.toLowerCase()) !== -1 && $(this).addClass('active');
      });
    });
  };

  $.fn['csSliderousel']['modules'].activePage = $.fn['csSliderousel']._activePage;

}(jQuery));